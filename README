NAME    reselect

SITE    https://git.khirnov.net/urxvt-ext-reselect.git/

AUTHOR  Anton Khirnov,
        based on url-picker by Chip Camden

DESCRIPTION

Perl extension for rxvt-unicode (aka urxvt) to enable quickly grabbing a regex match into
X11 clipboard. The design is similar to the 'follow hints' feature of Vimperator
(http://vimperator.org/vimperator).

INSTALLATION

See the man page for urxvtperl(3) for a full discussion of Perl extensions. Enable this
extension using one of the methods documented there. For instance, you could place the
script 'reselect' under ~/.urxvt/ext/, then add keybindings for it in .Xresources as

URxvt.keysym.M-u:    reselect:url

This will bind the 'Meta-u' key combination to the 'url' action, which selects URLs.

OPERATION

Each action has an associated regex pattern. The extension finds all matches of that
regex in visible text and assigns a short character sequence ('hint') to each of them.
The hints are displayed as overlays over the matches.

If no matches are found, the extesnion just pops up a warning overlay. The overlay
disappears after five seconds, but the terminal is immediately operable. If the region
contains only one match, it is immediately grabbed.

Typing the characters making up a hint grabs the corresponding match into clipboard. The
backspace key erases the last character typed, if there is one. Escape exits the
selection mode and removes all overlay windows, without doing anything.

When a match is grabbed, all overlay windows are removed and the cursor position is
restored to where it was in the terminal window before url-picker was invoked. The match
that was grabbed is displayed on the bottom row of the terminal window for five seconds.

ACTIONS

The following actions are defined by this extension:

url             Matches URLs.
git_hash        Mathes git commit hashes.
message_id      Matches angle-quote-enclosed email message IDs.
user.<n>        Matches the user-defined pattern number <n>. Patterns must be
                numbered consecutively.

RESOURCES

The following resources are recognized by url-picker:

URxvt.reselect.launchsingle     "false" to not auto-grab a single match
URxvt.reselect.order            "descending" to assign hints in reverse order
URxvt.reselect.pattern.<n>      define a custom pattern, which will be available
                                as the 'user.<n>' action

URxvt.reselect.input.backgroundColor    Bg color for characters typed
URxvt.reselect.input.foregroundColor    Fg color for characters typed
URxvt.reselect.hint.backgroundColor     Bg color for the hint overlays
URxvt.reselect.hint.foregroundColor     Fg color for the hint overlays
URxvt.reselect.prompt.backgroundColor   Bg color for "Follow:" prompt
URxvt.reselect.prompt.foregroundColor   Fg color for "Follow:" prompt
URxvt.reselect.status.backgroundColor   Bg color for status messages
URxvt.reselect.status.foregroundColor   Fg color for status messages

Colors must be specified using urxvt color indices as described in
the urxvt(1) man page under COLORS AND GRAPHICS.
